@extends('layouts.app')

@section('title', 'Contatos')

@section('content')

<div class="container-fluid background-contatos">
    <section>
        @include('layouts.breadcrumb-default')
    </section>

    <div class="container contatos-box pt-5 pb-5">
        <div class="row">
            <div class="col-lg-6">
                @if(App::getLocale() == 'en')
                    <p>TESTE</p>                
                @else
                <p>Em caso de dúvidas, informações ou sugestões, nos envie uma mensagem através do formulário abaixo.</p>
                @endif
                <div class="w-100"></div>

                <label for="nome" class="text-osan">*Nome</label>
                <div class="w-100"></div>
                <input class="w-100" type="text" name="nome" id="nome" required>
                <div class="w-100"></div>

                <label for="email" class="text-osan">*E-mail</label>
                <div class="w-100"></div>
                <input class="w-100" type="email" name="email" id="email" size="60" required>
                <div class="w-100"></div>

                <label for="telefone" class="text-osan">*Telefone</label>
                <div class="w-100"></div>
                <input class="w-100" type="tel" name="telefone" id="telefone" size="25" required>
                <div class="w-100"></div>

                <label for="depto" class="text-osan">*Departamento</label>
                <div class="w-100"></div>
                <select class="w-100" name="depto">
                    <option class="text-osan" value="departamentovendas" selected>Departamento de vendas</option>
                    <option class="text-osan" value="departamentocobranca">Departamento de Cobrança</option>
                    <option class="text-osan" value="cac">Central de Atendimento</option>
                </select>
                <div class="w-100"></div>

                <label for="assunto" class="text-osan">*Assunto</label>
                <div class="w-100"></div>
                <input class="w-100" type="text" name="assunto" id="assunto" size="60" required>
                <div class="w-100"></div>


                <label for="mensagem" class="text-osan">*Mensagem</label>
                <div class="w-100"></div>
                <textarea class="w-100" id="mensagem" name="mensagem" rows="8" cols="62" minlenght="10" required></textarea>
                <div class="w-100 mt-5"></div>

                <button class="text-uppercase border-0 pt-2 pb-2 pl-5 pr-5 rounded-pill button-blue send">enviar</button>
            </div>
            <div class="col-lg-5 offset-lg-1">

                <div class="d-none d-lg-block">
                    <button type="button" class="button-blue2 w-75 p-3 border-0 ml-4 rounded-top">
                        <div class="row">
                            <div class="col-lg-2">
                                <img src="{{asset('images/phone.png')}}">
                            </div>
                            <div class="col-lg-10">
                                <small>Em caso de falecimento ligue</small>
                                <div class="w-100"></div>
                                <p class="h3 text-center">0800-017 8000</p>
                            </div>
                        </div>
                    </button>
                </div>

                <div class="d-lg-none">
                    <div class="w-100 mt-5 mb-5"></div>
                    <div class="button-blue2 rounded-top">
                        <p class="h5 pt-1 text-center">Em caso de falecimento ligue</p>
                        <p class="h2 text-center">0800-017 8000</p>
                    </div>
                    <div class="w-100"></div>
                </div>

                <div class="w-100 mt-5 mb-5"></div>
                <p class="h5 text-osan">Outras formas de contato</p>
                <div class="w-100"></div>
                <small>Para maiores informações, seguem abaixo nossos endereços e telefones para contato a devida adesão.</small>

                <div class="w-100 pt-2 pb-3"></div>

                <p class="h5 text-osan">Central de Atendimento ao Cliente : CAC</p>
                <div class="w-100"></div>
                <small>Dúvidas, atendimentos, solicitações, cartão do associado.<br>Tel.: (13) 3228.8000 / atendimento@osan.com.br</small>

                <div class="w-100 pt-2 pb-3"></div>

                <p class="h5 text-osan">Departamento de Cobrança</p>
                <div class="w-100"></div>
                <small>Negociação de dívidas acima de 2 débitos.<br>Tel.: (13) 3228.8013 / faturamento@osan.com.br</small>

                <div class="w-100 pt-2 pb-3"></div>

                <p class="h5 text-osan">Plano Empresarial</p>
                <div class="w-100"></div>
                <small>Informações sobre inclusões e exclusões de associados, avisos de falecimentos, faturas para pagamentos e relatórios administrativos.<br>Tel.: (13) 3228.8019 / planoempresa@osan.com.br</small>

                <div class="w-100 pt-2 pb-3"></div>

                <p class="h5 text-osan">Departamento de Vendas</p>
                <div class="w-100"></div>
                <small>Tel.: (13) 3228.8001 / vendas@osan.com.br</small>

            </div>
        </div>
    </div>
</div>
</div>
@endsection