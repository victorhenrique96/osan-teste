@if(App::getLocale() == 'en')
<!-- FOOTER DESKTOP -->
<div class="d-none d-lg-block">

    <div class="container-fluid border-top">
        <div class="row">
        <div class="w-100 mt-3"></div>
            <div class="col-lg-4">
            </div>
            <div class="col-lg">
                <p>Acesso rápido</p>
            </div>
        </div>
<div class="container">
    <div class="row">
            <div class="col-lg-2">
                <a href="{{url('/')}}"><img src="{{asset('/images/logo.png')}}" alt="OSAN"></a>
            </div>
            <div class="col-lg-1 offset-lg-1">
                <ul class="footer-links">
                    <li class="text-uppercase"><a href="{{url('/a-osan')}}">osan</a></li>
                    <li class="text-uppercase"><a href="#">plans</a></li>
                    <li class="text-uppercase"><a href="{{url('/servicos')}}">services</a></li>
                    <li class="text-uppercase"><a href="{{url('/duvidas')}}">faq</a></li>
                    <li class="text-uppercase"><a href="{{url('/parceiros')}}">parts</a></li>
                </ul>
            </div>
            <div class="col-lg-2 footer-links">
                <ul class="footer-links">
                    <li class="text-uppercase"><a href="{{url('/depoimentos')}}">testimonials</a></li>
                    <li class="text-uppercase"><a href="{{url('/noticias')}}">news</a></li>
                    <li class="text-uppercase"><a href="{{url('/unidades')}}">units</a></li>
                    <li class="text-uppercase"><a href="{{url('/contatos')}}">contact</a></li>
                    <li class="text-uppercase"><a target="_blank" href="https://www.catho.com.br/empregos/osan">Work with us</a></li>
                </ul>
            </div>
            <div class="col-lg-4 footer-tel">
                <button type="button" class="btn button-light-blue rounded-pill d-flex">
                    <div class="row">
                        <div class="col-lg-12">
                            <small>In case of death call<br><span>0800-017 8000</span></small>
                        </div>
                    </div>
                </button>
                <button type="button" class="btn button-light-white rounded-pill">
                    <div class="row">
                        <div class="col-lg-2">
                            <img src="{{asset('/images/whatsapp-blue.png')}}" alt="WhatsApp">
                        </div>
                        <div class="col-lg-10">
                            <small>Talk to OSAN in WhatsApp</small><br><span class="telefone">(13) 99768870</span>
                        </div>
                    </div>
                </button>
                <button type="button" class="btn button-light-white rounded-pill">
                    <div class="row">
                        <div class="col-lg-2">
                            <img src="{{asset('/images/tel-blue.png')}}" alt="WhatsApp">
                        </div>
                        <div class="col-lg-10">
                            <small>Atendimento</small><br><span class="telefone">(13) 3228-8000</span>
                        </div>
                    </div>
                </button>
            </div>
            <div class="col-lg-2 footer-network">
                <p class="text-center">Social Networks</p>
                <div class="row mt-4">
                    <div class="col-lg-12 d-flex justify-content-between">
                        <i class="fab text-osan fa-facebook-f"></i>
                        <i class="fab text-osan fa-twitter"></i>
                        <i class="fab text-osan fa-linkedin-in"></i>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-12">
                        <small>Developed By <a href="www.kbrtec.com.br">KBRTEC</a></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<!-- FOOTER DESKTOP -->
@else
<!-- FOOTER DESKTOP -->
<div class="d-none d-lg-block">

    <div class="container-fluid border-top">
        <div class="row">
        <div class="w-100 mt-3"></div>
            <div class="col-lg-4">
            </div>
            <div class="col-lg">
                <p>Acesso rápido</p>
            </div>
        </div>
<div class="container">
    <div class="row">
            <div class="col-lg-2">
                <a href="{{url('/')}}"><img src="{{asset('/images/logo.png')}}" alt="OSAN"></a>
            </div>
            <div class="col-lg-1 offset-lg-1">
                <ul class="footer-links">
                    <li class="text-uppercase"><a href="{{url('/a-osan')}}">a osan</a></li>
                    <li class="text-uppercase"><a href="#">planos</a></li>
                    <li class="text-uppercase"><a href="{{url('/servicos')}}">serviços</a></li>
                    <li class="text-uppercase"><a href="{{url('/duvidas')}}">dúvidas</a></li>
                    <li class="text-uppercase"><a href="{{url('/parceiros')}}">parceiros</a></li>
                </ul>
            </div>
            <div class="col-lg-2 footer-links">
                <ul class="footer-links">
                    <li class="text-uppercase"><a href="{{url('/depoimentos')}}">depoimentos</a></li>
                    <li class="text-uppercase"><a href="{{url('/noticias')}}">notícias</a></li>
                    <li class="text-uppercase"><a href="{{url('/unidades')}}">unidades</a></li>
                    <li class="text-uppercase"><a href="{{url('/contatos')}}">contatos</a></li>
                    <li class="text-uppercase"><a target="_blank" href="https://www.catho.com.br/empregos/osan">trabalhe conosco</a></li>
                </ul>
            </div>
            <div class="col-lg-4 footer-tel">
                <button type="button" class="btn button-light-blue rounded-pill d-flex">
                    <div class="row">
                        <div class="col-lg-12">
                            <small>Em caso de falecimento ligue<br><span>0800-017 8000</span></small>
                        </div>
                    </div>
                </button>
                <button type="button" class="btn button-light-white rounded-pill">
                    <div class="row">
                        <div class="col-lg-2">
                            <img src="{{asset('/images/whatsapp-blue.png')}}" alt="WhatsApp">
                        </div>
                        <div class="col-lg-10">
                            <small>Fale conosco via WhatsApp</small><br><span class="telefone">(13) 99768870</span>
                        </div>
                    </div>
                </button>
                <button type="button" class="btn button-light-white rounded-pill">
                    <div class="row">
                        <div class="col-lg-2">
                            <img src="{{asset('/images/tel-blue.png')}}" alt="WhatsApp">
                        </div>
                        <div class="col-lg-10">
                            <small>Atendimento</small><br><span class="telefone">(13) 3228-8000</span>
                        </div>
                    </div>
                </button>
            </div>
            <div class="col-lg-2 footer-network">
                <p class="text-center">Acompanhe nas Redes Sociais</p>
                <div class="row mt-4">
                    <div class="col-lg-12 d-flex justify-content-between">
                        <i class="fab text-osan fa-facebook-f"></i>
                        <i class="fab text-osan fa-twitter"></i>
                        <i class="fab text-osan fa-linkedin-in"></i>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-12">
                        <small>Desenvolvido por <a href="www.kbrtec.com.br">KBRTEC</a></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<!-- FOOTER DESKTOP -->
@endif




<!-- FOOTER MOBILE -->
<div class="d-lg-none">

    <div class="container-fluid border-top">
            <div class="row">
                <div class="col-12">
                    <p>Acesso rápido</p>
                </div>
            </div>

            <div class="row">
                <div class="col-6 footer-links">
                    <ul>
                        <li class="text-uppercase"><a href="{{url('/a-osan')}}">a osan</a></li>
                        <li class="text-uppercase"><a href="#">planos</a></li>
                        <li class="text-uppercase"><a href="{{url('/servicos')}}">serviços</a></li>
                        <li class="text-uppercase"><a href="{{url('/duvidas')}}">dúvidas</a></li>
                        <li class="text-uppercase"><a href="{{url('/parceiros')}}">parceiros</a></li>
                    </ul>
                </div>
                <div class="col-6 footer-links">
                    <ul>
                        <li class="text-uppercase"><a href="{{url('/depoimentos')}}">depoimentos</a></li>
                        <li class="text-uppercase"><a href="{{url('/noticias')}}">notícias</a></li>
                        <li class="text-uppercase"><a href="{{url('/unidades')}}">unidades</a></li>
                        <li class="text-uppercase"><a href="{{url('/contatos')}}">contatos</a></li>
                        <li class="text-uppercase"><a target="_blank" href="https://www.catho.com.br/empregos/osan">trabalhe conosco</a></li>
                    </ul>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-6">
                    <p>Acompanhe nas Redes Sociais</p>
                </div>
                <div class="col-2">
                <i class="fab text-osan fa-facebook-f mr-5"></i>
                </div>

                <div class="col-2">
                <i class="fab text-osan fa-twitter"></i>
                </div>

                <div class="col-2">
                <i class="fab text-osan fa-linkedin-in"></i>
                </div>
            </div>
            
            <div class="row pt-2 pb-5 border-top">
                <div class="col-3">

                </div>
                <div class="col-6">
                    <small class="text-center">Desenvolvido por KBRTEC</small>
                </div>
                <div class="col-3">

                </div>
            </div>
<!-- FOOTER MOBILE -->

