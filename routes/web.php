<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/area-cliente', function () {
    return view('area-cliente');
});
Route::get('/a-osan', function () {
    return view('a-osan');
});
Route::get('/plano-empresarial', function () {
    return view('plano-empresarial');
});
Route::get('/plano-classico', function () {
    return view('plano-classico');
});
Route::get('/servicos', function () {
    return view('servicos');
});
Route::get('/servicos/translado-aereo', function () {
    return view('translado-aereo');
});
Route::get('/duvidas', function () {
    return view('duvidas');
});
Route::get('/parceiros', function () {
    return view('parceiros');
});
Route::get('/parceiros/bakery', function () {
    return view('parceiros-integra');
});
Route::get('/depoimentos', function () {
    return view('depoimentos');
});
Route::get('/noticias', function () {
    return view('noticias');
});
Route::get('/noticias-integra', function () {
    return view('noticias-integra');
});
Route::get('/unidades', function () {
    return view('unidades');
});
Route::get('/contatos', function () {
    return view('contatos');
});








