@extends('layouts.app')

@section('title', 'Plano Clássico')

@section('content')
<div class="container">
    <div class="row mb-2">
    <div class="col-lg-12">
        <small class="caminho">Você está em: Home /</i><span class="text-info"> @yield('title')</span></small>
    </div>
    </div>
    <div clas="row">
        <div class="col-lg-12">
            <div class="container">
                <img src="{{asset('images/planos/plano-classico.png')}}" class="img-fluid img-plano">
                <span class="plano-caption">@yield('title')</span>
                <span class="plano-caption-sub">de Assistência Funeral</span>
                <span class="text-osan h1 font-weight-bold mt-5 text-center d-lg-none d-block">@yield('title')</span>
            </div>
        </div>
    </div>

    <div class="row mt-5 quebra-linha">
        <div class="col-lg-5">
            <p class="text-planos">Criado para minimizar desconfortos, o <strong>Plano de Assistência Funeral</strong> oferece tranquilidade e evita a burocracia e os transtornos financeiros pertinentes à organização de um funeral. Através de atendimento humanizado, a OSAN proporciona segurança às famílias enlutadas, em caso de falecimento de ente querido. Dessa forma, você e sua família não precisarão se preocupar com nada, pois a OSAN realizará todos os procedimentos funerários necessários.
            </p>
            <p class="h5 text-osan font-weight-bold w-100"><i class="fas fa-user-friends mt-lg-5 pr-3 mt-3"></i>Dependentes</p>
            <p class="text-planos">Cada titular do Plano de Assistência Funeral tem direito a inscrever como dependentes: cônjuge ou companheira (o), pai, mãe, sogro, sogra e filhos solteiros.</p>

            <p class="h5 text-osan font-weight-bold w-100"><i class="fas fa-user-friends mt-lg-5 pr-3 mt-3"></i>Dependentes adicionais</p>
            <p class="text-planos">É possível incluir até dois dependentes adicionais no plano, independente do grau de parentesco. Para isso, haverá acréscimo de R$ 12,00 no valor da mensalidade de cada dependente.</p>

            <p class="h5 text-osan font-weight-bold w-100"><i class="fas fa-hand-holding-usd mt-lg-5 pr-3 mt-3"></i>Prazo e forma de pagamento</p>
            <p class="text-planos">A contratação do Plano de Assistência Funeral terá vigência de quatro anos, podendo
                ser renovada por igual período. A mensalidade do plano será de R$ 42,90, sendo a primeira a ser paga no ato da
                venda.A partir da segunda parcela, o titular do plano poderá escolher pagar dia 10 ou 22 de
                cada mês.</p>

            <div class="w-100"></div>

            <p class="h5 font-weight-bold text-osan"><i class="fas fa-phone-square-alt mt-lg-5 pr-3 mt-3"></i>Central de Atendimento 24 horas</p>
            <p class="text-planos">A OSAN disponibiliza aos associados uma Central de Atendimento 24 horas, que
                oferece informações sobre procedimentos funerários, recepciona comunicados de
                falecimento, toma providências referentes ao cerimonial e assessora a família durante
                todo o serviço.</p>
            <div class="w-100"></div>
            <p class="text-planos text-osan font-weight-bold">Em caso de falecimento, ligue gratuitamente para 0800 017 8000.</p>
        </div>
        <div class="col-lg-1 no-padmag">
        </div>
        <div clas="col-lg-6">
            <p class="h5 font-weight-bold text-osan w-100 text-cobertura">Cobertura:</p>
            <p class="text-planos-list">
                <ul class="text-list">
                    <li><i class="fas fa-check mr-2"></i>Urna sextavada, envernizada com alça varão e visor;</li>
                    <li><i class="fas fa-check mr-2"></i>Ornamentação;</li>
                    <li><i class="fas fa-check mr-2"></i>Paramentos para velório;</li>
                    <li><i class="fas fa-check mr-2"></i>Livro de Presença;</li>
                    <li><i class="fas fa-check mr-2"></i>Véu para urna;</li>
                    <li><i class="fas fa-check mr-2"></i>Velas próprias para velório;</li>
                    <li><i class="fas fa-check mr-2"></i>Uma Coroa de Flores (1 Bola);</li>
                    <li><i class="fas fa-check mr-2"></i>Higienização do corpo;</li>
                    <li><i class="fas fa-check mr-2"></i>Declaração de óbito;</li>
                    <li><i class="fas fa-check mr-2"></i>Guia de sepultamento;</li>
                    <li><i class="fas fa-check mr-2"></i>Agendamento junto ao Cemitério;</li>
                    <li><i class="fas fa-check mr-2"></i>Veículos para remoção e/ou cortejo dentro da abrangência geográfica;
                    <li><i class="fas fa-check mr-2"></i>Veículos para translados com franquia de 200 km percorridos (ida e volta);</li>
                    <li><i class="fas fa-check mr-2"></i>Serviço de café e biscoito nos primeiros momentos do velório;
                    <li><i class="fas fa-check mr-2"></i>Velório dentro da abrangência geográfica e de acordo com a disponibilidade do dia e hora nos Cemitérios Municipais;</li>
                </ul>
            </p>
            <div class="w-100"></div>
            <p class="h5 font-weight-bold text-osan w-100"><i class="fas fa-car mt-lg-3 pr-3 mt-1"></i>Carência</p>
            <p class="text-planos">Noventa dias após a data da venda do plano, desde que o contratante esteja com
                o pagamento em dia das parcelas mensais.</p>

            <div class="w-100"></div>
            <p class="h5 font-weight-bold text-osan w-100"><i class="fas fa-car mt-lg-5 pr-3 mt-3"></i>Transporte</p>
            <p class="text-planos">A <strong>OSAN</strong> possui ampla frota com mais de 20 automóveis disponíveis para servir o
                associado, além de veículos exclusivos para cortejos fúnebres realizados pela empresa
                na Baixada Santista.</p>
        </div>
    </div>

    <div class="row mt-5 mb-5">
        <div class="col-lg d-flex justify-content-center mb-3 mb-lg-0">
            <button class="rounded-pill button-blue text-uppercase pb-2 pt-2 pl-5 pr-5 border-0 text-white">Contratar este plano</button>
        </div>

        <div class="col-lg d-flex justify-content-center">
            <a href="{{url('/plano-empresarial')}}"><button class="rounded-pill button-blue text-uppercase pb-2 pt-2 pl-5 pr-5 border-0 text-white">Conheça também o plano empresarial</button></a>
        </div>
    </div>

</div>
@endsection