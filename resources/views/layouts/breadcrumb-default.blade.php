<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <small class="caminho">Você está em: Home /</i><span class="text-info"> @yield('title')</span></small>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <p class="titulo">@yield('title')</p>
            <div class="w-100 mt-5"></div>
        </div>
    </div>
</div>