@extends('layouts.app')

@section('title', 'Unidades')

@section('content')
<div class="container-fluid background-unidades">
    <section>
        @include('layouts.breadcrumb-default')
    </section>

    <div class="container unidades-box">
        <div class="card-deck">
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/unidades/santos.png')}}" alt="Santos">
                <div class="card-body">
                    <div class="d-flex justify-content-end">
                        <a href="https://www.google.com" class="btn btn-outline-info" target="_blank">Ver no Mapa</a>
                    </div>
                    <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Santos</h5>
                    <p class="card-text">Rua Quinze de Novembro, 165 Centro - Santos</p>
                    <span class="card-link text-osan"><i class="fas fa-phone-square-alt"></i> (13) 3228.8000</span>
                </div>
            </div>

            <div class="card">
                <img class="card-img-top" src="{{asset('/images/unidades/sao-vicente.png')}}" alt="São Vicente">
                <div class="card-body">
                    <div class="d-flex justify-content-end">
                        <a href="https://www.google.com" class="btn btn-outline-info" target="_blank">Ver no Mapa</a>
                    </div>
                    <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>São Vicente</h5>
                    <p class="card-text">Rua Padre Anchieta, 224 - Centro Santos<br>Rua Lima Machado, 501 - Parque Bitaru (Rede Conveniada)</p>
                    <span class="card-link text-osan"><i class="fas fa-phone-square-alt"></i> (13) 3228.8000</span>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/unidades/cubatao.png')}}" alt="Cubatão">
                <div class="card-body">
                    <div class="d-flex justify-content-end">
                        <a href="https://www.google.com" class="btn btn-outline-info" target="_blank">Ver no Mapa</a>
                    </div>
                    <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Cubatão</h5>
                    <p class="card-text">Av. Martins Fontes, 125 - Vila Nova - Santos (Rede Conveniada)</p>
                    <span class="card-link text-osan"><i class="fas fa-phone-square-alt"></i> (13) 3228.8000</span>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/unidades/praia-grande.png')}}" alt="Praia Grande">
                <div class="card-body">
                    <div class="d-flex justify-content-end">
                        <a href="https://www.google.com" class="btn btn-outline-info" target="_blank">Ver no Mapa</a>
                    </div>
                    <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Praia Grande</h5>
                    <p class="card-text">Rua Aimorés, 238 - Vl. Tupi - Santos<br>Rua Maria do Carmo Ferro Gomes Ornellas, 82 - Vila Antártica - Santos (Rede Conveniada)</p>
                    <span class="card-link text-osan"><i class="fas fa-phone-square-alt"></i> (13) 3228.8000</span>
                </div>
            </div>
        </div>
        <div class="card-deck">
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/unidades/guaruja.png')}}" alt="Guarujá">
                <div class="card-body">
                    <div class="d-flex justify-content-end">
                        <a href="https://www.google.com" class="btn btn-outline-info" target="_blank">Ver no Mapa</a>
                    </div>
                    <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Guarujá</h5>
                    <p class="card-text">Av. Marco Antônio Oggiano, 24 Morrinhos - Guarujá</p>
                    <span class="card-link text-osan"><i class="fas fa-phone-square-alt"></i> (13) 3228.8000</span>
                </div>
            </div>

            <div class="card">
                <img class="card-img-top" src="{{asset('/images/unidades/itanhaem.png')}}" alt="Itanhaém">
                <div class="card-body">
                    <div class="d-flex justify-content-end">
                        <a href="https://www.google.com" class="btn btn-outline-info" target="_blank">Ver no Mapa</a>
                    </div>
                    <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Itanhaém</h5>
                    <p class="card-text">Av. Rui Barbosa, 810 Centro - Santos</p>
                    <span class="card-link text-osan"><i class="fas fa-phone-square-alt"></i> (13) 3228.8000</span>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/unidades/mongagua.png')}}" alt="Mongaguá">
                <div class="card-body">
                    <div class="d-flex justify-content-end">
                        <a href="https://www.google.com" class="btn btn-outline-info" target="_blank">Ver no Mapa</a>
                    </div>
                    <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Mongaguá</h5>
                    <p class="card-text">Av. Marina, 1106 Centro - Santos</p>
                    <span class="card-link text-osan"><i class="fas fa-phone-square-alt"></i> (13) 3228.8000</span>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/unidades/bertioga.png')}}" alt="Bertioga">
                <div class="card-body">
                    <div class="d-flex justify-content-end">
                        <a href="https://www.google.com" class="btn btn-outline-info" target="_blank">Ver no Mapa</a>
                    </div>
                    <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Bertioga</h5>
                    <p class="card-text">Rua Leonardo de Bonna, 39 - Itapanhaú - Santos</p>
                    <span class="card-link text-osan"><i class="fas fa-phone-square-alt"></i> (13) 3228.8000</span>
                </div>
            </div>
        </div>
    </div>
</div>

    @endsection