@extends('layouts.app')

@section('title', 'Serviços')

@section('content')
<div class="container-fluid background-servicos">
    <section>
        @include('layouts.breadcrumb-subtitle')
    </section>
    <div class="container servicos-box">
        <div class="container servicos servicos-page">
            <!-- CARD DECK -->
            <div class="card-deck mt-3">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/servico-funeral.png')}}" alt="Serviço Funerário">
                    <div class="card-body">
                        <h5 class="card-title">Serviço Funerário</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                        <button type="button" class="btn btn-outline-primary">+ SAIBA MAIS</button>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/translado-terrestre.png')}}" alt="Translado Terrestre">
                    <div class="card-body">
                        <h5 class="card-title">Translado Terrestre</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                        <button type="button" class="btn btn-outline-primary">+ SAIBA MAIS</button>
                    </div>
                </div>
                <div class="card">
                    <a href="{{url('servicos/translado-aereo')}}">
                        <img class="card-img-top" src="{{asset('/images/translado-aereo.png')}}" alt="Translado Aéreo"></a>
                    <div class="card-body">
                        <h5 class="card-title">Translado Aéreo</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                        <a href="{{url('servicos/translado-aereo')}}"><button type="button" class="btn btn-outline-primary">+ SAIBA MAIS</button></a>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/embalsamento formolizacao.png')}}" alt="Embalsamento Formolização">
                    <div class="card-body">
                        <h5 class="card-title text-nowrap">Embalsamento Formolização</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                        <button type="button" class="btn btn-outline-primary">+ SAIBA MAIS</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- CARD DECK -->
    </div>

</div>
</div>

@endsection