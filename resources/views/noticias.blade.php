@extends('layouts.app')

@section('title', 'Notícias')

@section('content')
<div class="container-fluid background-noticias">
    <section>
        @include('layouts.breadcrumb-with-searchbar')
    </section>


    <div class="container noticias-box">
        <div class="container new-noticias">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-deck">
                        <div class="card">
                            <img src="{{asset('/images/noticias/noticia-1.png')}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text"><small class="text-muted">17 de maio de 2017</small></p>
                                <h5 class="card-title text-uppercase font-weight-light">Programação para o dia da lembrança 2018</h5>
                                <button type="button" class="btn btn-outline-dark rounded-pill text-uppercase">+ ler mais</button>
                            </div>
                        </div>
                        <div class="card">
                            <img src="{{asset('/images/noticias/noticia-2.png')}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text"><small class="text-muted">17 de maio de 2017</small></p>
                                <h5 class="card-title text-uppercase font-weight-light">Meditação como auxílio de superação do luto</h5>
                                <button type="button" class="btn btn-outline-dark rounded-pill text-uppercase">+ ler mais</button>
                            </div>
                        </div>
                        <div class="card">
                            <img src="{{asset('/images/noticias/noticia-3.png')}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text"><small class="text-muted">17 de maio de 2017</small></p>
                                <h5 class="card-title text-uppercase font-weight-light">O que fazer com os pertences de um falecido?</h5>
                                <button type="button" class="btn btn-outline-dark rounded-pill text-uppercase">+ ler mais</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-12">
                    <div class="card-deck">
                        <div class="card">
                            <img src="{{asset('/images/noticias/noticia-4.png')}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text"><small class="text-muted">17 de maio de 2017</small></p>
                                <h5 class="card-title text-uppercase font-weight-light">Porque homenagear com flores?</h5>
                                <button type="button" class="btn btn-outline-dark rounded-pill text-uppercase">+ ler mais</button>
                            </div>
                        </div>
                        <div class="card">
                            <img src="{{asset('/images/noticias/noticia-5.png')}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text"><small class="text-muted">17 de maio de 2017</small></p>
                                <h5 class="card-title text-uppercase font-weight-light">Lidando com o luto de um colega de trabalho</h5>
                                <a href="{{url('/noticias-integra')}}">
                                    <button type="button" class="btn btn-outline-dark rounded-pill text-uppercase">+ ler mais</button>
                                </a>
                            </div>
                        </div>
                        <div class="card">
                            <img src="{{asset('/images/noticias/noticia-6.png')}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text"><small class="text-muted">17 de maio de 2017</small></p>
                                <h5 class="card-title text-uppercase font-weight-light">diferentes formas de perder o medo da morte</h5>
                                <button type="button" class="btn btn-outline-dark rounded-pill text-uppercase">+ ler mais</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section>
        @include('layouts.pagination')
    </section>
    @endsection