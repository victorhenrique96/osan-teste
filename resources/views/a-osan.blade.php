@extends('layouts.app')

@section('title', 'A OSAN')

@section('content')
<div class="container-fluid background-depoimentos">
    <section>
        @include('layouts.breadcrumb-default')
    </section>


    <div class="container parceiros-box mr-auto ml-auto">
        <div class="row">
        <span class="text-justify">
        OSAN, Sinônimo de Bons Serviços Prestados à Baixada Santista.
        A Organização Social de Ataúdes Nóvoa – OSAN, atualmente está consolidada e é reconhecida por profissionais capacitados que humanizam o atendimento. A sua história é marcada por muito trabalho, dedicação e respeito aos milhares de associados. Os fundadores da OSAN, Sr. Manoel Rodriguez Nóvoa e sua esposa Marina Gonzalez Lopez, naturais da Espanha, inauguraram em 1960 uma pequena fábrica de urnas. Em 1965, o espírito de luta de seus fundadores transformou a pequena fábrica em um empreendimento próspero, bem organizado e totalmente dirigido à família: a Organização Social de Ataúdes Nóvoa.

        O casal e seu único filho, Manoel Rodriguez Gonzalez, carinhosamente chamado de Manolo, dirigiam a empresa que expandia suas atividades, iniciadas em Santos, para outros municípios como Praia Grande, São Vicente, Mongaguá, Itanhaém, Cubatão e Guarujá.

        No início dos anos 90 começa uma nova etapa para os negócios da família. Manolo e sua esposa Cátia, assumem a administração da empresa. Surge, então, o Plano de Assistência familiar OSAN.

        Nesta época, a empresa sofre uma reestruturação física e as unidades são informatizadas, a frota de veículos totalmente renovada e a filosofia organizacional repensada para atender, ainda melhor, aos seus clientes. Outros pontos de atendimento foram inaugurados, o que gerou muitos empregos diretos e indiretos.

        Atualmente, a marca OSAN está consolidada e é reconhecida pela seriedade e qualidade dos serviços que presta. Prova disso é o mais de meio milhão de associados ao Plano de Assistência Familiar, que conta com uma equipe formada por profissionais capacitados que humanizam o atendimento, em respeito a você.
        </span>
        </div>
    </div>
</div>
@endsection