@extends('layouts.app')

@if(App::getLocale() == 'en')
@section('title', 'Home')
@else
@section('title', 'Início')
@endif


@section('content')

@if(App::getLocale() == 'en')
<!-- SLIDE -->
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="background-carousel"></div>
            <div class="carousel-caption d-md-block">
                <h5 class="text-primary osan">OSAN</h5>
                <p class="text-dark imp">Check out our plans</p>
                <p class="text-dark plan">The Osan's Plan is a leader in funeral assistance. Call Us<br>and we will take care of everything for you.</p>
                <button type="button" class="btn button-light-blue2 rounded-pill float-left">Online Budget</button>
            </div>

        </div>
        <div class="carousel-item">
            <div class="background-carousel"></div>
            <div class="carousel-caption d-md-block">
                <h5 class="text-primary osan">OSAN</h5>
                <p class="text-dark imp">Check out our plans</p>
                <p class="text-dark plan">The Osan's Plan is a leader in funeral assistance. Call Us<br>and we will take care of everything for you.</p>
                <button type="button" class="btn button-light-blue2 rounded-pill float-left">Online Budget</button>
            </div>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"><img src="{{asset('/images/seta-esquerda.png')}}">
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <img src="{{asset('/images/seta-direita.png')}}">
        </a>
    </div>
</div>
<!-- SLIDE -->
@else
<!-- SLIDE -->
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="background-carousel"></div>
            <div class="carousel-caption d-md-block">
                <h5 class="text-primary osan">OSAN</h5>
                <p class="text-dark imp">Confira nossos planos</p>
                <p class="text-dark plan">O plano Osan é líder em assistência funeral. É só ligar<br>que cuidaremos de tudo para você.</p>
                <button type="button" class="btn button-light-blue2 rounded-pill float-left">Orçamento Online</button>
            </div>

        </div>
        <div class="carousel-item">
            <div class="background-carousel"></div>
            <div class="carousel-caption d-md-block">
                <h5 class="text-primary osan">OSAN</h5>
                <p class="text-dark imp">Confira nossos planos</p>
                <p class="text-dark plan">O plano Osan é líder em assistência funeral. É só ligar<br>que cuidaremos de tudo para você.</p>
                <button type="button" class="btn button-light-blue2 rounded-pill float-left">Orçamento Online</button>
            </div>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"><img src="{{asset('/images/seta-esquerda.png')}}">
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <img src="{{asset('/images/seta-direita.png')}}">
        </a>
    </div>
</div>
<!-- SLIDE -->
@endif

<div class="w-100 mt-5"></div>

@if(App::getLocale() == 'en')
<!-- DEPOIMENTOS -->
<div class="container">
    <div class="row">
        <div class="col-lg">
            <p class="text-center titulo-cap text-uppercase">Testimonial</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg">
            <p class="text-center subtitulo-cap">What our customers say</p>
        </div>
    </div>
    <div class="row comentarios">
        <div class="col-lg-4"><img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
            <span class="font-italic text-secondary">Thank you so much for everything. You proved to be a professional expert, with great tact and delicacy, excellent energy. All of this brought immense relief to the family.
            </span>
            <small class="nome-familia font-weight-bold text-uppercase">Devid and family</small>
        </div>
        <div class="col-lg-4"><img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
            <span class="font-italic text-secondary">Thank you so much for everything. You proved to be a professional expert, with great tact and delicacy, excellent energy. All of this brought immense relief to the family.
            </span>
            <small class="nome-familia font-weight-bold text-uppercase">- Lucas evedove</small>
        </div>
        <div class="col-lg-4"><img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
            <span class="font-italic text-secondary">Thank you so much for everything. You proved to be a professional expert, with great tact and delicacy, excellent energy. All of this brought immense relief to the family.
            </span>
            <small class="nome-familia font-weight-bold text-uppercase">Devid and family</small>
        </div>
    </div>
</div>
<div class="w-100 depoimentos-padding"></div>
<!-- DEPOIMENTOS -->
@else
<!-- DEPOIMENTOS -->
<div class="container">
    <div class="row">
        <div class="col-lg">
            <p class="text-center titulo-cap text-uppercase">Depoimentos</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg">
            <p class="text-center subtitulo-cap">O que nossos clientes dizem</p>
        </div>
    </div>
    <div class="row comentarios">
        <div class="col-lg-4"><img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
            <span class="font-italic text-secondary">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.
            </span>
            <small class="nome-familia font-weight-bold text-uppercase">- Devid e família</small>
        </div>
        <div class="col-lg-4"><img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
            <span class="font-italic text-secondary">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.
            </span>
            <small class="nome-familia font-weight-bold text-uppercase">- Lucas evedove</small>
        </div>
        <div class="col-lg-4"><img class="float-left mr-4" src="{{asset('/images/chat-like.png')}}">
            <span class="font-italic text-secondary">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.
            </span>
            <small class="nome-familia font-weight-bold text-uppercase">Devid e família</small>
        </div>
    </div>
</div>
<div class="w-100 depoimentos-padding"></div>
<!-- DEPOIMENTOS -->
@endif

<div class="w-100 mt-5"></div>

@if(App::getLocale() == 'en')
<!-- NOTICIAS -->
<div class="container-fluid d-none d-lg-block noticias">
    <div class="row">
        <div class="col-lg">
            <p class="titulo-cap text-white text-center mt-4">News</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg">
            <p class="subtitulo-cap text-center text-white">Check out our news</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 no-padmag">
                <div class="img-fruid">
                    <img src="{{asset('/images/noticias/img-1.png')}}">
                    <span class="text-white noticias-caption">Schedule<br>for 2018's<br>Remembrance Day</span>
                </div>
            </div>
            <div class="col-lg-6 no-padmag">
                <div class="img-fruid">
                    <img src="{{asset('/images/noticias/img-2.png')}}">
                    <span class="text-white noticias-caption">Meditation as<br>a help overcoming<br>of mourning</span>
                </div>                    
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 no-padmag">
                <div class="img-fruid">
                    <img src="{{asset('/images/noticias/img-3.png')}}">
                    <span class="text-white noticias-caption">Learn why<br>honor with<br>flowers?</span>
                </div>
            </div>
            <div class="col-lg-4 no-padmag">
                <div class="img-fruid">
                    <img src="{{asset('/images/noticias/img-4.png')}}">
                    <span class="text-white noticias-caption">What to do<br>with the belongings<br>of a deceased?</span>
                </div> 
            </div>
            <div class="col-lg-4 no-padmag">
                <div class="img-fruid">
                    <img src="{{asset('/images/noticias/img-5.png')}}">
                    <span class="text-white noticias-caption">What's the best method<br>to communicate<br>a death?</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- NOTICIAS -->

<!-- NOTICIAS mobile -->
<div class="container-fluid d-lg-none noticias">
    <div class="row">
        <div class="col-lg">
            <p class="titulo-cap text-white text-center mt-3">News</p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg">
            <p class="subtitulo-cap text-center text-white">Check out our news</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <img class="mobile-noticias" src="{{asset('/images/noticias/img-1.png')}}">
            <span class="text-white noticias-caption">Schedule<br>for 2018's<br>Remembrance Day</span>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <img class="mobile-noticias" src="{{asset('/images/noticias/img-2.png')}}">
            <span class="text-white noticias-caption">Meditation as<br>a help overcoming<br>of mourning</span>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <img class="mobile-noticias" src="{{asset('/images/noticias/img-3.png')}}">
            <span class="text-white noticias-caption">Learn why<br>honor with<br>flowers?</span>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <img class="mobile-noticias" src="{{asset('/images/noticias/img-4.png')}}">
            <span class="text-white noticias-caption">What to do<br>with the belongings<br>of a deceased?</span>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <img class="mobile-noticias" src="{{asset('/images/noticias/img-5.png')}}">
            <span class="text-white noticias-caption">What's the best method<br>to communicate<br>a death?</span>
        </div>
    </div>

</div>
</div>
<!-- NOTICIAS mobile -->
@else
<!-- NOTICIAS -->
<div class="container-fluid d-none d-lg-block noticias">
    <div class="row">
        <div class="col-lg">
            <p class="titulo-cap text-white text-center mt-4">Notícias</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg">
            <p class="subtitulo-cap text-center text-white">Confira nossas ultimas notícias</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 no-padmag">
                <div class="img-fruid">
                    <img src="{{asset('/images/noticias/img-1.png')}}">
                    <span class="text-white noticias-caption">Programação<br>para o dia da<br>Lembrança 2018</span>
                </div>
            </div>
            <div class="col-lg-6 no-padmag">
                <div class="img-fruid">
                    <img src="{{asset('/images/noticias/img-2.png')}}">
                    <span class="text-white noticias-caption">Meditação como<br>auxílio de superação<br>do luto</span>
                </div>                    
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 no-padmag">
                <div class="img-fruid">
                    <img src="{{asset('/images/noticias/img-3.png')}}">
                    <span class="text-white noticias-caption">Saiba porque<br>homenagear<br>com flores?</span>
                </div>
            </div>
            <div class="col-lg-4 no-padmag">
                <div class="img-fruid">
                    <img src="{{asset('/images/noticias/img-4.png')}}">
                    <span class="text-white noticias-caption">O que fazer com<br>os pertences de<br>um falecido?</span>
                </div> 
            </div>
            <div class="col-lg-4 no-padmag">
                <div class="img-fruid">
                    <img src="{{asset('/images/noticias/img-5.png')}}">
                    <span class="text-white noticias-caption">Qual a melhor forma<br>de comunicar uma<br>morte?</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- NOTICIAS -->

<!-- NOTICIAS mobile -->
<div class="container-fluid d-lg-none noticias">
    <div class="row">
        <div class="col-lg">
            <p class="titulo-cap text-white text-center mt-3">Notícias</p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg">
            <p class="subtitulo-cap text-center text-white">Confira nossas ultimas notícias</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <img class="mobile-noticias" src="{{asset('/images/noticias/img-1.png')}}">
            <span class="text-white noticias-caption">Programação<br>para o dia da<br>Lembrança 2018</span>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <img class="mobile-noticias" src="{{asset('/images/noticias/img-2.png')}}">
            <span class="text-white noticias-caption">Meditação como<br>auxílio de superação<br>do luto</span>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <img class="mobile-noticias" src="{{asset('/images/noticias/img-3.png')}}">
            <span class="text-white noticias-caption">Saiba porque<br>homenagear<br>com flores?</span>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <img class="mobile-noticias" src="{{asset('/images/noticias/img-4.png')}}">
            <span class="text-white noticias-caption">O que fazer com<br>os pertences de<br>um falecido?</span>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <img class="mobile-noticias" src="{{asset('/images/noticias/img-5.png')}}">
            <span class="text-white noticias-caption">Qual a melhor forma<br>de comunicar uma<br>morte?</span>
        </div>
    </div>

</div>
</div>
<!-- NOTICIAS mobile -->
@endif


<div class="w-100 mt-5"></div>

@if(App::getLocale() == 'en')
<!-- SERVIÇOS -->
<div class="container-fluid servicos">
    <div class="row">
        <div class="col-lg">
            <p class="titulo-cap text-center mt-3">Services</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg">
            <p class="subtitulo-cap text-center">Know our services</p>
        </div>
    </div>
    <!-- CARD DECK -->
    <div class="container">
        <div class="card-deck mt-3">
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/servico-funeral.png')}}" alt="Serviço Funerário">
                <div class="card-body">
                    <h5 class="card-title">Funeral service</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                    <button type="button" class="btn btn-outline-primary">+ LEARN MORE</button>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/translado-terrestre.png')}}" alt="Translado Terrestre">
                <div class="card-body">
                    <h5 class="card-title">Ground transfer</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                    <button type="button" class="btn btn-outline-primary">+ LEARN MORE</button>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/translado-aereo.png')}}" alt="Translado Aéreo">
                <div class="card-body">
                    <h5 class="card-title">Air transfer</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                    <a href="{{url('servicos/translado-aereo')}}"><button type="button" class="btn btn-outline-primary">+ LEARN MORE</button></a>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/embalsamento formolizacao.png')}}" alt="Embalsamento Formolização">
                <div class="card-body">
                    <h5 class="card-title">Embalming formaldehyde</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                    <button type="button" class="btn btn-outline-primary">+ LEARN MORE</button>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100 mt-5"></div>
</div>
<!-- CARD DECK -->
@else
<!-- SERVIÇOS -->
<div class="container-fluid servicos">
    <div class="row">
        <div class="col-lg">
            <p class="titulo-cap text-center mt-3">Serviços</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg">
            <p class="subtitulo-cap text-center">Conheçam os serviços que oferecemos</p>
        </div>
    </div>
    <!-- CARD DECK -->
    <div class="container">
        <div class="card-deck mt-3">
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/servico-funeral.png')}}" alt="Serviço Funerário">
                <div class="card-body">
                    <h5 class="card-title">Serviço Funerário</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                    <button type="button" class="btn btn-outline-primary">+ SAIBA MAIS</button>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/translado-terrestre.png')}}" alt="Translado Terrestre">
                <div class="card-body">
                    <h5 class="card-title">Translado Terrestre</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                    <button type="button" class="btn btn-outline-primary">+ SAIBA MAIS</button>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/translado-aereo.png')}}" alt="Translado Aéreo">
                <div class="card-body">
                    <h5 class="card-title">Translado Aéreo</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                    <a href="{{url('servicos/translado-aereo')}}"><button type="button" class="btn btn-outline-primary">+ SAIBA MAIS</button></a>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="{{asset('/images/embalsamento formolizacao.png')}}" alt="Embalsamento Formolização">
                <div class="card-body">
                    <h5 class="card-title">Embalsamento Formolização</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                    <button type="button" class="btn btn-outline-primary">+ SAIBA MAIS</button>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100 mt-5"></div>
</div>
<!-- CARD DECK -->
@endif

@if(App::getLocale() == 'en')
<!-- UNIDADES -->
<div class="container-fluid border-top">
    <div class="container unidades">
        <div class="w-100 mt-5"></div>
        <div class="row">
            <div class="col-lg">
                <p class="titulo-cap text-center">Service Units</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <p class="subtitulo-cap text-center">Find the nearest unit to you</p>
            </div>
        </div>
        <ul>
            <div class="card-deck">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/santos.png')}}" alt="Santos">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Santos</h5>
                    </div>
                </div>

                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/sao-vicente.png')}}" alt="São Vicente">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>São Vicente</h5>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/cubatao.png')}}" alt="Cubatão">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Cubatão</h5>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/praia-grande.png')}}" alt="Praia Grande">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Praia Grande</h5>
                    </div>
                </div>
            </div>
            <div class="card-deck">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/guaruja.png')}}" alt="Guarujá">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Guarujá</h5>
                    </div>
                </div>

                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/itanhaem.png')}}" alt="Itanhaém">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Itanhaém</h5>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/mongagua.png')}}" alt="Mongaguá">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Mongaguá</h5>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/bertioga.png')}}" alt="Bertioga">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Bertioga</h5>
                    </div>
                </div>
            </div>
        </ul>
        <div class="w-100 mt-5"></div>
    </div>
<!-- UNIDADES -->
@else
<!-- UNIDADES -->
<div class="container-fluid border-top">
    <div class="container unidades">
        <div class="w-100 mt-5"></div>
        <div class="row">
            <div class="col-lg">
                <p class="titulo-cap text-center">Unidades</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                <p class="subtitulo-cap text-center">Encontre a unidade mais próxima</p>
            </div>
        </div>
        <ul>
            <div class="card-deck">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/santos.png')}}" alt="Santos">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Santos</h5>
                    </div>
                </div>

                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/sao-vicente.png')}}" alt="São Vicente">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>São Vicente</h5>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/cubatao.png')}}" alt="Cubatão">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Cubatão</h5>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/praia-grande.png')}}" alt="Praia Grande">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Praia Grande</h5>
                    </div>
                </div>
            </div>
            <div class="card-deck">
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/guaruja.png')}}" alt="Guarujá">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Guarujá</h5>
                    </div>
                </div>

                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/itanhaem.png')}}" alt="Itanhaém">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Itanhaém</h5>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/mongagua.png')}}" alt="Mongaguá">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Mongaguá</h5>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top" src="{{asset('/images/unidades/bertioga.png')}}" alt="Bertioga">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-map-marker-alt mr-2"></i>Bertioga</h5>
                    </div>
                </div>
            </div>
        </ul>
        <div class="w-100 mt-5"></div>
    </div>
<!-- UNIDADES -->
@endif

@endsection